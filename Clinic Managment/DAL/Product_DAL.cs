﻿
using ChartJSCore.Models;
using Clinic_Managment.Models;
using Microsoft.CodeAnalysis.Elfie.Diagnostics;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using static System.Collections.Specialized.BitVector32;
using static System.Net.Mime.MediaTypeNames;

namespace Clinic_Managment.DAL
{
    public class Product_DAL
    {

        private readonly Userdata _context;
        
        //Stored Procedures
        string conString = new SqlConnection("Data Source=.;Initial Catalog=Manageamets;Persist Security Info=True;Integrated Security=SSPI;App=SampleApp").ConnectionString;
        //Get List Product
        public List<Product> GetAllProduct()
        { 
             List <Product> ProductsList = new List<Product>();
            using(SqlConnection Connection = new SqlConnection(conString)){
               SqlCommand command = Connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "spt_GetAllProducts";
                SqlDataAdapter SqlIDA = new SqlDataAdapter(command);
                DataTable dataProduct = new DataTable();
                Connection.Open();
                SqlIDA.Fill(dataProduct);
                Connection.Close();
                foreach(DataRow dr in dataProduct.Rows)
                {
                    ProductsList.Add(new Product
                    { 
                        ProductID = Convert.ToInt32(dr["ProductID"]),
                        ProductName = dr["ProductName"].ToString(),
                        Price = Convert.ToDecimal(dr["Price"]),
                        Qty = Convert.ToInt32(dr["Qty"]),
                        Remarks = dr["Remarks"].ToString(),
                    }); 
                }
            }
            return ProductsList;
         }
        // Add Product
        public int AddProduct(Product product)
        {
            int ProductID;

            using (SqlConnection mySqlConn = new SqlConnection(conString))
            {
                mySqlConn.Open();

                using (SqlCommand myComm = new SqlCommand("spts_InsertProducts", mySqlConn))
                {
                    myComm.CommandType = CommandType.StoredProcedure;
                    myComm.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = product.ProductName;
                    myComm.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;
                    myComm.Parameters.Add("@Qty", SqlDbType.Int).Value = product.Qty;
                    myComm.Parameters.Add("@Remarks", SqlDbType.NVarChar).Value = product.Remarks;
                    myComm.Parameters.Add("@ProductID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    myComm.ExecuteNonQuery();
                    ProductID = Convert.ToInt32(myComm.Parameters["@ProductID"].Value);
                    mySqlConn.Close();
                }
            }

            return ProductID;
        }
        public Product UpdateProduct(Product product)
        {
            int id = 0;
            using (SqlConnection Connection = new SqlConnection(conString))
            {
                SqlCommand command = new SqlCommand("sp_UpdateProducts", Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ProductID", product.ProductID);
                command.Parameters.AddWithValue("@ProductName", product.ProductName);
                command.Parameters.AddWithValue("@Price", product.Price);
                command.Parameters.AddWithValue("@Qty", product.Qty);
                command.Parameters.AddWithValue("@Remarks", product.Remarks);
                Connection.Open();
                id = command.ExecuteNonQuery();
                Connection.Close();
                return product;
            }
        }
        public string DeleteProduct(int productid)
        {
            string result = "";
            using (SqlConnection Connection = new SqlConnection(conString))
            {
                SqlCommand command = new SqlCommand("sp_DeleteProducts", Connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@productid", productid);
                command.Parameters.Add("@OUTPUTMESSAGE", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                Connection.Open();
                command.ExecuteNonQuery();
                result = command.Parameters["@OUTPUTMESSAGE"].Value.ToString();
                Connection.Close();
               
            }
            return result;
        }
        public List<Product> getProduct(string serchproduct)
        {
            List<Product> ProductsList = new List<Product>();
            SqlConnection Connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand("stppt_SearchProductss", Connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ProductName", serchproduct);
            var result = new DataTable();
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(result);
            foreach (DataRow dr in result.Rows)
            {
                ProductsList.Add(new Product
                {
                    ProductID = Convert.ToInt32(dr["ProductID"]),
                    ProductName = dr["ProductName"].ToString()
                });
            }
            return ProductsList;
        }

        public List<Product> SearchProduct(string idstr)
        {
            List<Product> ProductsList = new List<Product>();
            SqlConnection Connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand("stppt_SearchProducsstss", Connection);
            command.CommandType = CommandType.StoredProcedure;
            var stringArray = idstr.Split(",");
            int[] intArray = new int[stringArray.Length];

            for (int index = 0; index < stringArray.Length; index++)
            {
                int.TryParse(stringArray[index], out intArray[index]);

            }
            for (int index = 0; index < intArray.Length; index++)
            {
                command.Parameters.Add("@ProductID", SqlDbType.VarChar).Value = intArray[index];
            };
            Connection.Open();
            command.ExecuteNonQuery();
            command.Parameters.Clear();
            SqlDataReader sdr = command.ExecuteReader();
              ProductsList.Add(new Product
              {
                  ProductID = Convert.ToInt32(sdr["ProductID"]),
                  ProductName = sdr["ProductName"].ToString(),
                  Price = Convert.ToDecimal(sdr["Price"]),
                  Qty = Convert.ToInt32(sdr["Qty"]),
                  Remarks = sdr["Remarks"].ToString(),
              });
            Connection.Open();

            return ProductsList;
        }
    }
}
