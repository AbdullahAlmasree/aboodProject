﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Clinic_Managment.Migrations
{
    /// <inheritdoc />
    public partial class firstMigrationrg6464 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "UserLogins");

            migrationBuilder.DropColumn(
                name: "MobilrNumber",
                table: "UserLogins");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "UserLogins",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "MobilrNumber",
                table: "UserLogins",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
