﻿using Clinic_Managment.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Clinic_Managment
{
    public class VisitEditor
    {
        public int ID { get; set; }
        [BindProperty, DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime? VistDate { get; set; }

        [Required(ErrorMessage = "Please enter your Docotor")]
        public int doctorId { get; set; }

        [Required(ErrorMessage = "Please enter your Patient")]
        public int patientId { get; set; }
        [Required(ErrorMessage = "Please enter your Clinic")]
        public int clinicID { get; set; }

        [Required(ErrorMessage = "Please enter your Docotor")]
        [Display(Name = "doctorName")]
        [DataType(DataType.Text)]
        public string doctorName { get; set; }

        [Required(ErrorMessage = "Please enter your Patient")]
        [Display(Name = "patientName")]
        [DataType(DataType.Text)]
        public string patientName { get; set; }
        [Required(ErrorMessage = "Please enter your Clinic")]
        [Display(Name = "ClinicName")]
        [DataType(DataType.Text)]
        public string ClinicName { get; set; }


    }
}
