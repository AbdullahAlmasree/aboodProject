using Clinic_Managment.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Clinic_Managment.Areas.Managment.Controllers;
using System.Security.Cryptography;
using Microsoft.CodeAnalysis.Options;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Twilio.Clients;
using Twillio.Services;
using Clinic_Managment.Helpers;
using Clinic_Managment.Services;
using System.Reflection;
using System.Configuration;
using Clinic_Managment.Areas.Managment.IRepositry;
using Clinic_Managment.Areas.Managment.Repositry;
using NuGet.Protocol.Core.Types;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(options =>
{
    options.ModelMetadataDetailsProviders.Add(new SystemTextJsonValidationMetadataProvider());
});
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));

builder.Services.AddScoped<IStudentRepositry, StudentRepositry>();


builder.Services.AddControllersWithViews();
builder.Services.AddDbContext<Userdata>
    (options =>
options.UseSqlServer(
     builder.Configuration.
     GetConnectionString("Manageamets")));

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddAuthentication(
CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(option =>
    {
        option.LoginPath = "/Managment/Login/Login";
        option.ExpireTimeSpan = TimeSpan.FromMinutes(3);
    });

builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromHours(20);
    });
builder.Services.AddControllers();

builder.Services.Configure<TwilioSetting>(builder.Configuration.GetSection("Twilio"));
builder.Services.AddTransient<ISMSServices,SMSServices>();
var app = builder.Build();
app.UseSession();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
app.UseDeveloperExceptionPage();
app.UseHttpsRedirection();
app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.MapControllerRoute(
    name: "MyArea",
    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");


app.Run();

