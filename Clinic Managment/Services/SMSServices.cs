﻿using Clinic_Managment.Dtos;
using Clinic_Managment.Helpers;
using Clinic_Managment.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Clinic_Managment.Services
{
  
    public class SMSServices : ISMSServices
    {
        private readonly TwilioSetting _twilio;

        public SMSServices(IOptions<TwilioSetting> twilio)
        {
            _twilio = twilio.Value;
        }
      
        public MessageResource Sendsmss(string mobile, string bodyfg, string code)
        {
                Random random = new Random();
                int value = random.Next(1001, 9999);
                var Code = code = value.ToString();
                string bodyss = "YOur Code Is" + Code;
                var body = bodyfg = HttpUtility.UrlEncode(bodyss);
                TwilioClient.Init(_twilio.AccountSID, _twilio.AuthToken);

                var result = MessageResource.Create(
                    body: body,
                    from: new Twilio.Types.PhoneNumber(_twilio.TwilioPhoneNumber),
                    to: mobile
                    );
                return result;
        }
    }
}
