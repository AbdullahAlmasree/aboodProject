﻿using Clinic_Managment.Dtos;
using Clinic_Managment.Models;
using Twilio.Rest.Api.V2010.Account;

namespace Clinic_Managment.Services
{
    public interface ISMSServices
    {
        MessageResource Sendsmss(string mobile, string bodyfg, string code);
    }
}
