﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic_Managment.Dtos
{
    public class SendSmsDto
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int idcodes { get; set; }
        public string MobileNumber { get; set; }
        public string Body { get; set; }
        public string Code { get; set; }
    }
}
