﻿using Clinic_Managment.Areas.Managment.IRepositry;
using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authentication;

namespace Clinic_Managment.Areas.Managment.Repositry
{
    public class StudentRepositry : IStudentRepositry
    {
        private Userdata _dbDbContext;
        public IAuthenticationService _authenticationService;
        public StudentRepositry(Userdata dbDbContext)
        {
            _dbDbContext = dbDbContext;
        }
        public StudentModel EditStudentModel(int studentid)
        {
            var StudentModels = new StudentModel();
            if (studentid == 0)
            {
                StudentModels.Name = string.Empty;
            }
            else
            {
                StudentModels = _dbDbContext.studentModels
                           .Where(p => p.StudentId == studentid)
                           .ToList().Select(
                             s => new StudentModel()
                             {
                                 StudentId = s.StudentId,
                                 Name = s.Name,
                             }).FirstOrDefault();
            }
            return StudentModels;
        }
        public StudentModel SaveStudentModel(StudentModel studentModel)
        {
            if (studentModel.StudentId == 0)
            {
                studentModel =  _dbDbContext.Add(studentModel).Entity;
                _dbDbContext.SaveChanges();
            }
            else
            {
                studentModel =  _dbDbContext.Update(studentModel).Entity;
                _dbDbContext.SaveChanges();
            }
            return studentModel;
        }

    }

}
