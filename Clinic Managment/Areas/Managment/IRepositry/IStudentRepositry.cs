﻿using Clinic_Managment.Models;
using MediatR;

namespace Clinic_Managment.Areas.Managment.IRepositry
{
    public interface IStudentRepositry
    {
        public StudentModel EditStudentModel(int studentid);
        public StudentModel SaveStudentModel(StudentModel? studentModel);

    }
}
