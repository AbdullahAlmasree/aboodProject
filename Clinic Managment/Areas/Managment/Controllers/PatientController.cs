﻿using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.DependencyResolver;
using System.Numerics;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class PatientController : Controller
    {
    
        private readonly Userdata _context;
        public PatientController(Userdata context)
        {
            _context = context;

        }
        public IActionResult PatientIndex()
        {
            List<Patient> Patientlist = new List<Patient>();
            var patient = _context.Patient;
            Patientlist = (from patients in patient
                          select patients).ToList();
            return View(Patientlist);

        }
        public JsonResult GetPatient(string searchpatient , string selectpatientid)
        {
            List<Patient> Patients = new List<Patient>();
            if (!string.IsNullOrEmpty(selectpatientid))
            {
                var ids = selectpatientid.Split(',').Select(Int32.Parse).ToList();
                Patients = _context.Patient.Where(x => !ids.Contains(x.patientId)).ToList();
            }
            else
            {
                Patients = _context.Patient.Where(x => x.Name.Contains(searchpatient)).ToList();
            }
            var modifieData = Patients.Select(x => new
            {
                id = x.patientId,
                text = x.Name
            });
            return Json(modifieData);
        }


        public IActionResult SearchPatient(string idsStr)
        {
            List<Patient> SearchList = new List<Patient>();

            if (!string.IsNullOrEmpty(idsStr))
            {
                var ids = idsStr.Split(',').Select(Int32.Parse).ToList();
                SearchList = (from n in _context.Patient
                              where ids.Contains(n.patientId)
                              select n).ToList();
            }
            else
            {
                SearchList = (from n in _context.Patient

                              select n).ToList();
            }

            return PartialView("SearchPatient", SearchList);
        }

        [HttpPost]
        public IActionResult DeletePatient(int teacherID)
        {
            try
            {


                Patient com = _context.Patient.Where(x => x.patientId == teacherID).FirstOrDefault();
                _context.Patient.Remove(com);
                _context.SaveChanges();


                return Ok(new { success = true });
            }

            catch (Exception)
            {
                return Json(new { success = false });
            }
        }
        public IActionResult GetDetailsPatient(int? id)
        {

            Patient obj = new Patient();

            obj = (from y in _context.Patient
                   where y.patientId == id
                   select y).FirstOrDefault();
            return View("GetDetailsPatient", obj);

        }
        public IActionResult EditPatient(int? id)
        {

            return View(_context.Patient.Find(id));
        }

        [HttpPost]
        public IActionResult EditPatient(Patient patient)
        {
            if (_context.Patient.Any(a => a.Name.Equals(patient.Name)))
            {
                return Ok("patientnames");
            }
            else
            {
                if (patient.patientId == 0)
                {
                    _context.Entry(patient).State = EntityState.Added;
                    _context.SaveChanges();
                }
                else
                {
                    _context.Entry(patient).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                return PartialView("PatientItem", patient);
            }
        }
    }
}
