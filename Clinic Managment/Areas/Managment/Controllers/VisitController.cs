﻿using Clinic_Managment.Models;
using Humanizer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;
using Microsoft.SqlServer.Server;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web.WebPages;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class VisitController : Controller
    {
        private readonly Userdata _context;
        public VisitController(Userdata context)
        {
            _context = context;
        }
      
        public IActionResult VisitIndex()
        {
            var model = new List<VisitsModels>();
            model = _context.Visits
            .Select(x => new VisitsModels
            {
                ID = x.ID,
                VistDate = x.VistDate,
                DoctorId = x.doctorId,
                PatientId = x.patientId,
                ClinicID = x.clinicID,
                DoctorName = x.Doctor.Name,
                PatientName = x.Patient.Name,
                Clinic = x.Clinic.clinic
            })
            .ToList();
            return View(model);

        }
      public JsonResult Getdoctor(string doctorname)
        {
            var doctorList = _context.Doctor.Where(x => x.Name.Contains(doctorname)).ToList();
            var doctorData = doctorList.Select(x => new
            {
                id = x.doctorId,
                text = x.Name
            });
            return Json(doctorData);
        }
        public JsonResult GetPatient(string Patientname)
        {
            var PatientList = _context.Patient.Where(x => x.Name.Contains(Patientname)).ToList();
            var PatientData = PatientList.Select(x => new
            {
                id = x.patientId,
                text = x.Name
            });
            return Json(PatientData);
        }
        public JsonResult GetClinic(string Clinictname)
        {
            var ClinicPatientList = _context.Clinic.Where(x => x.clinic.Contains(Clinictname)).ToList();
            var ClinicData = ClinicPatientList.Select(x => new
            {
                id = x.clinicID,
                text = x.clinic
            });
            return Json(ClinicData);
        }


        public JsonResult Getvisits(string vists)
        {
            var listvisit = _context.Visits
             .Where(x => x.Doctor.Name.Contains(vists.ToUpper()))
             .Select(x => new
            {
                id =  x.doctorId,
                text = x.Doctor.Name,
            }).ToList();
            return Json(listvisit);
        }

        public JsonResult Getvisitss(string vistss)
        {
            var listvisita = _context.Visits
             .Where(x => x.Patient.Name.Contains(vistss.ToUpper()))
             .Select(x => new
             {
                 id = x.patientId,
                 text = x.Patient.Name,
             }).ToList();
            return Json(listvisita);
        }


        public JsonResult Getvisitssss(string vistssss)
        {
            var listvisist = _context.Visits
             .Where(x => x.Clinic.clinic.Contains(vistssss.ToUpper()))
             .Select(x => new
             {
                 id = x.clinicID,
                 text = x.Clinic.clinic,
             }).ToList();
            return Json(listvisist);
        }
        public IActionResult visitlist(string doctors ,  string patient, string clinic ,DateTime startdate, DateTime enddate)
        {
            var model = new List<VisitsModels>();
            var SearchList = new Visit();
            if (!string.IsNullOrEmpty(doctors) && !string.IsNullOrEmpty(patient) && !string.IsNullOrEmpty(clinic))
            {
                var doctorids = doctors.Split(',').Select(Int32.Parse).ToList();
                var patientids = patient.Split(',').Select(Int32.Parse).ToList();
                var clinicids = clinic.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => doctorids.Contains(x.doctorId) && patientids.Contains(x.patientId) && clinicids.Contains(x.clinicID))
                    .Select(x => new VisitsModels
                    {
                        ID = x.ID,
                        VistDate = x.VistDate,
                        DoctorId = x.doctorId,
                        PatientId = x.patientId,
                        ClinicID = x.clinicID,
                        DoctorName = x.Doctor.Name,
                        PatientName = x.Patient.Name,
                        Clinic = x.Clinic.clinic
                    }).ToList();
            }

            else if (string.IsNullOrEmpty(doctors) && !string.IsNullOrEmpty(patient) && !string.IsNullOrEmpty(clinic))
            {
                var patientids = patient.Split(',').Select(Int32.Parse).ToList();
                var clinicids = clinic.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => patientids.Contains(x.patientId) && clinicids.Contains(x.clinicID))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }


            else if (!string.IsNullOrEmpty(doctors) && string.IsNullOrEmpty(patient) && !string.IsNullOrEmpty(clinic))
            {
                var doctorids = doctors.Split(',').Select(Int32.Parse).ToList();
                var clinicids = clinic.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => doctorids.Contains(x.doctorId) && clinicids.Contains(x.clinicID))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }


            else if (!string.IsNullOrEmpty(doctors) && !string.IsNullOrEmpty(patient) && string.IsNullOrEmpty(clinic))
            {
                var doctorids = doctors.Split(',').Select(Int32.Parse).ToList();
                var patientids = patient.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => doctorids.Contains(x.doctorId) && patientids.Contains(x.patientId))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }


            else if (string.IsNullOrEmpty(doctors) && string.IsNullOrEmpty(patient) && string.IsNullOrEmpty(clinic) && string.IsNullOrEmpty(startdate.ToString()) && string.IsNullOrEmpty(enddate.ToString()))
            {
                model = _context.Visits
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }

            else if (!string.IsNullOrEmpty(doctors) && string.IsNullOrEmpty(patient) && string.IsNullOrEmpty(clinic))
            {
                var doctorids = doctors.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => doctorids.Contains(x.doctorId))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }

            else if (string.IsNullOrEmpty(doctors) && !string.IsNullOrEmpty(patient) && string.IsNullOrEmpty(clinic))
            {
                var patientids = patient.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => patientids.Contains(x.patientId))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }

            else if (string.IsNullOrEmpty(doctors) && string.IsNullOrEmpty(patient) && !string.IsNullOrEmpty(clinic))
            {
                var clinicids = clinic.Split(',').Select(Int32.Parse).ToList();
                model = _context.Visits.Where(x => clinicids.Contains(x.clinicID))
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }

            else if (string.IsNullOrEmpty(doctors) && string.IsNullOrEmpty(patient) && string.IsNullOrEmpty(clinic) && !string.IsNullOrEmpty(startdate.ToString()) && !string.IsNullOrEmpty(enddate.ToString()))
            {

                model = _context.Visits.Where(x => x.VistDate >= startdate && x.VistDate <= enddate)
                                     .Select(x => new VisitsModels
                                     {
                                         ID = x.ID,
                                         VistDate = x.VistDate,
                                         DoctorId = x.doctorId,
                                         PatientId = x.patientId,
                                         ClinicID = x.clinicID,
                                         DoctorName = x.Doctor.Name,
                                         PatientName = x.Patient.Name,
                                         Clinic = x.Clinic.clinic
                                     }).ToList();
            }
            return PartialView("visitlist", model);
        }
        public IActionResult GetDetailsVisits(int? visitID)

        {

            var visit = _context.Visits.Where(x => x.ID == visitID)
            .Select(x => new VisitsModels
            {
                ID = x.ID,
                VistDate = x.VistDate,
                DoctorId = x.doctorId,
                PatientId = x.patientId,
                ClinicID = x.clinicID,
                DoctorName = x.Doctor.Name,
                PatientName = x.Patient.Name,
                Clinic = x.Clinic.clinic
            }).FirstOrDefault();
            return View("GetDetailsVisits", visit);
        }
        [HttpPost]

        public IActionResult DeleteVisits(int visitID)
        {
            try
            {
                Visit visit = _context.Visits.Where(x => x.ID == visitID).FirstOrDefault();
                _context.Visits.Remove(visit);
                _context.SaveChanges();
                return Ok(new { success = true });
            }
            catch (Exception)
            {
                return Ok(new { success = false });
            }
        }

        public IActionResult AddVisits(int? visitID)
        {
            var visitEditor = new VisitEditor();

            if (visitID.HasValue)
            {
                visitEditor = _context.Visits.Where(x => x.ID == visitID)
                    .Select(x => new VisitEditor
                    {
                        ID = x.ID,
                        VistDate = x.VistDate,
                        doctorId = x.doctorId,
                        patientId = x.patientId,
                        clinicID = x.clinicID,
                        doctorName = x.Doctor.Name,
                        patientName = x.Patient.Name,
                        ClinicName = x.Clinic.clinic
                    }).FirstOrDefault();
            }
            return View(visitEditor);
        }

        [HttpPost]
        public IActionResult AddVisits(VisitEditor visitEditors)
        {
            var visitsModel = new VisitsModels();
            var visit = new Visit();
                visit.VistDate = (DateTime)visitEditors.VistDate;
                visit.doctorId = visitEditors.doctorId;
                visit.patientId = visitEditors.patientId;
                visit.clinicID = visitEditors.clinicID;
                _context.Add(visit);
                _context.SaveChanges();
                visitsModel = _context.Visits.Where(v => v.ID == visit.ID)
                               .Select(x => new VisitsModels
                               {
                                   ID = x.ID,
                                   VistDate = x.VistDate,
                                   DoctorId = x.doctorId,
                                   PatientId = x.patientId,
                                   ClinicID = x.clinicID,
                                   DoctorName = x.Doctor.Name,
                                  PatientName = x.Patient.Name,
                                   Clinic = x.Clinic.clinic
                               }).FirstOrDefault();
                return PartialView("VisitItem", visitsModel);
        }

        public IActionResult EditVisits(int? visitID)
        {
            var visitEditor = new VisitEditor();
          
            if (visitID.HasValue)
            {
                visitEditor = _context.Visits.Where(x => x.ID == visitID)
                    .Select(x => new VisitEditor
                     {
                         ID = x.ID,
                         VistDate = x.VistDate,
                         doctorId = x.doctorId,
                         patientId = x.patientId,
                         clinicID = x.clinicID,
                         doctorName = x.Doctor.Name,
                         patientName = x.Patient.Name,
                         ClinicName = x.Clinic.clinic
                     }).FirstOrDefault();
            }
            return View(visitEditor);
        }
        [HttpPost]
        public IActionResult EditVisits(VisitEditor visitEditor)
        {
            var visitsModel = new VisitsModels();
            var visit = new Visit();
                visit.ID = visitEditor.ID;
                visit.VistDate = (DateTime)visitEditor.VistDate;
                visit.doctorId = visitEditor.doctorId;
                visit.patientId = visitEditor.patientId;
                visit.clinicID = visitEditor.clinicID;
                _context.Update(visit);
                _context.SaveChanges();
                visitsModel = _context.Visits.Where(v => v.ID == visit.ID)
                               .Select(x => new VisitsModels
                               {
                                   ID = x.ID,
                                   VistDate = x.VistDate,
                                   DoctorId = x.doctorId,
                                   PatientId = x.patientId,
                                   ClinicID = x.clinicID,
                                   DoctorName = x.Doctor.Name,
                                   PatientName = x.Patient.Name,
                                   Clinic = x.Clinic.clinic
                               }).FirstOrDefault();
                return PartialView("VisitItem", visitsModel);
        }
        public IActionResult Clear(string doctorss , string patientss , string clinicss , DateTime startdatess , DateTime enddatess)
        {
            var model = new List<VisitsModels>();
            if (string.IsNullOrEmpty(doctorss) && string.IsNullOrEmpty(patientss) && string.IsNullOrEmpty(clinicss) && string.IsNullOrEmpty(startdatess.ToString()) && string.IsNullOrEmpty(enddatess.ToString()))
            {

                model = _context.Visits.Where(x => x.VistDate >= startdatess && x.VistDate <= enddatess)
                                     .Select(x => new VisitsModels
                                     {
                                         ID = x.ID,
                                         VistDate = x.VistDate,
                                         DoctorId = x.doctorId,
                                         PatientId = x.patientId,
                                         ClinicID = x.clinicID,
                                         DoctorName = x.Doctor.Name,
                                         PatientName = x.Patient.Name,
                                         Clinic = x.Clinic.clinic
                                     }).ToList();
            }



            else if (string.IsNullOrEmpty(doctorss) && string.IsNullOrEmpty(patientss) && string.IsNullOrEmpty(clinicss) && !string.IsNullOrEmpty(startdatess.ToString()) && !string.IsNullOrEmpty(enddatess.ToString()))
            {

                model = _context.Visits
                       .Select(x => new VisitsModels
                       {
                           ID = x.ID,
                           VistDate = x.VistDate,
                           DoctorId = x.doctorId,
                           PatientId = x.patientId,
                           ClinicID = x.clinicID,
                           DoctorName = x.Doctor.Name,
                           PatientName = x.Patient.Name,
                           Clinic = x.Clinic.clinic
                       }).ToList();
            }

            return PartialView("visitlist", model);
        }
    }
}
