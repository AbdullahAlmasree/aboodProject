﻿using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly Userdata _context;
        public UsersController(Userdata context)
        {
            _context = context;

        }
        public IActionResult Usersindex()
        {
            var model = new UserDashboardModel();
            model.UserLogins = _context.UserLogins.ToList();
            model.TotalActiveUser = model.UserLogins.Count(x => x.IsValid == true);

            model.TotalNotActiveUser = model.UserLogins.Count(x => x.IsValid == false);
            return View(model);
        }

        public JsonResult GetUser(string searchName , string selectuserid)
        {
            List<UserLogin> users = new List<UserLogin>();
            if (!string.IsNullOrEmpty(selectuserid))
            {
                var ids = selectuserid.Split(',').Select(Int32.Parse).ToList();
                users = _context.UserLogins.Where(x => !ids.Contains(x.ID)).ToList();

            }
            else
            {
              users = _context.UserLogins.Where(x => x.FirstName.Contains(searchName)).ToList();
            }

            var modifieData = users .Select(x => new
            {
                id = x.ID,
                text = x.FirstName
            });
            return Json(modifieData);
        }


        public IActionResult SearchUsers(string idsStr)
        {
            List<UserLogin> SearchList = new List<UserLogin>();

            if (!string.IsNullOrEmpty(idsStr))
            {
                var ids = idsStr.Split(',').Select(Int32.Parse).ToList();
                SearchList = (from n in _context.UserLogins
                              where ids.Contains(n.ID)
                              select n).ToList();
            }
            else
            {
                SearchList = (from n in _context.UserLogins

                              select n).ToList();
            }

            return PartialView("SearchUsers", SearchList);
        }

        [HttpPost]
        public IActionResult DeleteUsers(int teacherID)
        {
                UserLogin redirectdeletre = _context.UserLogins.Where(x => x.ID == teacherID).FirstOrDefault();
                if (HttpContext.Session.GetString("username") != redirectdeletre.Username)
                {
                    UserLogin com = _context.UserLogins.Where(x => x.ID == teacherID).FirstOrDefault();
                    _context.UserLogins.Remove(com);
                    _context.SaveChanges();

                    return Ok("true");
                }
                else
                {
                    UserLogin com = _context.UserLogins.Where(x => x.ID == teacherID).FirstOrDefault();
                    _context.UserLogins.Remove(com);
                    _context.SaveChanges();
                    HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return Ok("Return");
                }
        }
        private string Encrypt_Password(string password)
        {
            string pswstr = string.Empty;
            byte[] psw_encode = new byte[password.Length];
            psw_encode = System.Text.Encoding.UTF8.GetBytes(password);
            pswstr = Convert.ToBase64String(psw_encode);
            return pswstr;
        }
        private string DecryptPasswordBase64(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public IActionResult GetDetailsUsers(int? id)
        {

            UserLogin obj = new UserLogin();

            obj = (from y in _context.UserLogins
                   where y.ID == id
                   select y).FirstOrDefault();
           obj.Password = DecryptPasswordBase64(obj.Password);
            return View("GetDetailsUsers", obj);

        }
        public IActionResult EditUsers(int? id)
        {
            UserLogin obj = new UserLogin();
            obj = (from y in _context.UserLogins
                   where y.ID == id
                   select y).FirstOrDefault();
            obj.Password = DecryptPasswordBase64(obj.Password);
            return View(obj);
        }
        public ActionResult Activate(int id)
        {
            string Status = "";

            UserLogin Data = _context.UserLogins.Where(x => x.ID == id).FirstOrDefault();
            Data.IsValid = true;
            _context.SaveChanges();
            return RedirectToAction("Usersindex");
        }
         

        public ActionResult Deactivate(int id)
        {
            string Status = "";
            UserLogin Data = _context.UserLogins.Where(x => x.ID == id).FirstOrDefault();
            Data.IsValid = false;
            _context.SaveChanges();
            return RedirectToAction("Usersindex");
        }

        [HttpPost]
        public IActionResult EditUsers(UserLogin userLogin)
        {
            if (_context.UserLogins.Any(a => a.Username.Equals(userLogin.Username)))
            {
                return Ok("dublicateuses");
            }
            if (_context.UserLogins.Any(a => a.Email.Equals(userLogin.Email)))
            {
                return Ok("dublicateemail");
            }
            if (_context.UserLogins.Any(a => a.FirstName.Equals(userLogin.FirstName)))
            {
                return Ok("dublicatefirst");
            }
            if (_context.UserLogins.Any(a => a.Password.Equals(Encrypt_Password(userLogin.Password))))
            {
                return Ok("dublicatePassword");
            }
            if (userLogin.ID == 0)
            {
                userLogin.ResetPasswordCode = "";
                userLogin.Body = "";
                userLogin.Code = "";
                userLogin.MobileNumber = "";
                userLogin.IsValid = true;
                userLogin.Password = Encrypt_Password(userLogin.Password);
                _context.Entry(userLogin).State = EntityState.Added;
                _context.SaveChanges();
            }
            else
            {
                userLogin.ResetPasswordCode = "";
                userLogin.Body = "";
                userLogin.Code = "";
                userLogin.MobileNumber = "";
                userLogin.IsValid = true;
                _context.Entry(userLogin).State = EntityState.Modified;
                userLogin.Password = Encrypt_Password(userLogin.Password);
                _context.SaveChanges();
            }
            return PartialView("UsersItem", userLogin);
        }

        public IActionResult Changepassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Changepassword(ChangPaswordModel changPasword)
        {
            var item = Encrypt_Password(changPasword.NewPassword);

            if (_context.UserLogins.Any(a => a.Password.Equals(item)))
            {
                ViewBag.Messas = "Please, the Password is in the database, choose another Password";
            }
            else
            {
                var items = Encrypt_Password(changPasword.OldPassword);
                if (changPasword.NewPassword != changPasword.ConfirmPassword)
                {
                    ViewBag.MessasNewPassword = "Please, the new password and confirm They must be similar";

                }
                else
                {
                    var change = _context.UserLogins.Where(x => x.Password == items).FirstOrDefault();
                    if (change == null)
                    {
                        ViewBag.Messases = "Password Is Not Correct";
                    }
                    else
                    {
                        var userId = int.Parse(HttpContext.Session.GetString("ID"));
                        var detail = _context.UserLogins.Where(log => log.Password == Encrypt_Password(changPasword.OldPassword)).FirstOrDefault();
                        if (detail != null)
                        {
                            var userDetail = _context.UserLogins.FirstOrDefault(c => c.ID == userId);


                            if (userDetail != null)
                            {
                                userDetail.Password = Encrypt_Password(changPasword.NewPassword);

                                _context.SaveChanges();
                                ViewBag.Messagess = "Record Inserted Successfully!";
                            }
                            else
                            {
                                ViewBag.Messagess = "Password not Updated!";
                            }

                        }
                    }
                }
            }
     
            return View(changPasword);
        }
        
    
    }
}
