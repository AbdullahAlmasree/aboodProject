﻿using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class ClinicController : Controller
    {
        private readonly Userdata _context;
        public ClinicController(Userdata context)
        {
            _context = context;

        }
        public IActionResult ClinicIndex()
        {
            List<Clinic> Cliniclist = new List<Clinic>();
            var clinic = _context.Clinic;
            Cliniclist = (from clincs in clinic
                          select clincs).ToList();
            return View(Cliniclist);

        }
        public JsonResult GetClinic(string searchclinicss , string selectclinicid)
        {
            List<Clinic> clinics = new List<Clinic>();
            if (!string.IsNullOrEmpty(selectclinicid))
            {
                var ids = selectclinicid.Split(',').Select(Int32.Parse).ToList();
                clinics = _context.Clinic.Where(x => !ids.Contains(x.clinicID)).ToList();

            }
            else
            {
                clinics = _context.Clinic.Where(x => x.clinic.Contains(searchclinicss)).ToList();

            }
            var modifieData = clinics.Select(x => new
            {
                id = x.clinicID,
                text = x.clinic
            });
            return Json(modifieData);
        }


        public IActionResult SearchClinic(string idsStr)
        {
            List<Clinic> SearchList = new List<Clinic>();

            if (!string.IsNullOrEmpty(idsStr))
            {
                var ids = idsStr.Split(',').Select(Int32.Parse).ToList();
                SearchList = (from n in _context.Clinic
                              where ids.Contains(n.clinicID)
                              select n).ToList();
            }
            else
            {
                SearchList = (from n in _context.Clinic

                              select n).ToList();
            }

            return PartialView("SearchClinic", SearchList);
        }

        [HttpPost]
        public IActionResult DeleteClinic(int teacherID)
        {
            try
            {


                Clinic com = _context.Clinic.Where(x => x.clinicID == teacherID).FirstOrDefault();
                _context.Clinic.Remove(com);
                _context.SaveChanges();


                return Ok(new { success = true });
            }

            catch (Exception)
            {
                return Json(new { success = false });
            }
        }
        public IActionResult GetDetailsClinic(int? id)
        {

            Clinic obj = new Clinic();

            obj = (from y in _context.Clinic
                   where y.clinicID == id
                   select y).FirstOrDefault();
            return View("GetDetailsClinic", obj);

        }
        public IActionResult EditClinic(int? id)
        {

            return View(_context.Clinic.Find(id));
        }

        [HttpPost]
        public IActionResult EditClinic(Clinic clinic)
        {
            if (_context.Clinic.Any(a => a.clinic.Equals(clinic.clinic)))
            {
                return Ok("clinicnames");
            }
            else
            {
                if (clinic.clinicID == 0)
                {
                    _context.Entry(clinic).State = EntityState.Added;
                    _context.SaveChanges();
                }
                else
                {
                    _context.Entry(clinic).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                return PartialView("ClinicItem", clinic);
            }
 
        }
    }
}
