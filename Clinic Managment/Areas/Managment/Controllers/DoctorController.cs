﻿using ChartJSCore.Plugins.Zoom;
using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.DependencyResolver;
using NuGet.Protocol.Plugins;
using System.Numerics;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class DoctorController : Controller
    {
        private readonly Userdata _context;
        public DoctorController(Userdata context)
        {
            _context = context;
        }
        public IActionResult DoctorIndex()
        {
            List<Doctor> Doctorlist = new List<Doctor>();
           var doctor = _context.Doctor;
            Doctorlist = (from Doctors in doctor
                           select Doctors).ToList();
            return View(Doctorlist);

        }
        public JsonResult GetDoctors(string searchdoctor , string selectdoctorid)
        {
                List<Doctor> Doctors = new List<Doctor>();
            if (!string.IsNullOrEmpty(selectdoctorid)){
                var ids = selectdoctorid.Split(',').Select(Int32.Parse).ToList();
                Doctors = _context.Doctor.Where(x => !ids.Contains(x.doctorId)).ToList();
            }
            else
            {
                Doctors = _context.Doctor.Where(x => x.Name.Contains(searchdoctor)).ToList();
            }
            var modifieData = Doctors.Select(x => new
            {
                id = x.doctorId,
                text = x.Name
            });
            return Json(modifieData);
        }


        public IActionResult SearchDoctor(string idsStr)
        {
            List<Doctor> SearchList = new List<Doctor>();

            if (!string.IsNullOrEmpty(idsStr))
            {
                var ids = idsStr.Split(',').Select(Int32.Parse).ToList();
                SearchList = (from n in _context.Doctor
                              where ids.Contains(n.doctorId)
                              select n).ToList();
            }
            else
            {
                SearchList = (from n in _context.Doctor

                              select n).ToList();
            }

            return PartialView("SearchDoctor", SearchList);
        }

        [HttpPost]
        public IActionResult DeleteDoctor(int teacherID)
        {
            try
            {


                Doctor com = _context.Doctor.Where(x => x.doctorId == teacherID).FirstOrDefault();
                _context.Doctor.Remove(com);
                _context.SaveChanges();


                return Ok(new { success = true });
            }

            catch (Exception)
            {
                return Json(new { success = false });
            }
        }
        public IActionResult GetDetailsDoctor(int? id)
        {

            Doctor obj = new Doctor();

            obj = (from y in _context.Doctor
                   where y.doctorId == id
                   select y).FirstOrDefault();
            return View("GetDetailsDoctor", obj);

        }
        public IActionResult EditDoctor(int? id)
        {

            return View(_context.Doctor.Find(id));
        }

        [HttpPost]
        public IActionResult EditDoctor(Doctor doctor)
        {
            if (_context.Doctor.Any(a => a.Name.Equals(doctor.Name)))
            {
                return Ok("doctornames");
            }
            else
            {
                if (doctor.doctorId == 0)
                {
                    _context.Entry(doctor).State = EntityState.Added;
                    _context.SaveChanges();
                }
                else
                {
                    _context.Entry(doctor).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                return PartialView("DoctorItem", doctor);
            }
        }
    }
}
