﻿using Clinic_Managment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.DependencyResolver;
using System;
using System.Data.Entity;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly Userdata _context;
        public readonly Microsoft.AspNetCore.Identity.UserManager<IdentityUser> _userManager;
        public UserController(Userdata context)
        {
            _context = context;
            
        }

        public IActionResult UserIndex()
        {
            var model = new UserDashboardModel();
            model.Users = _context.Users.ToList();
            model.TotalActiveUser = model.Users.Count(x => x.Status == true);

            model.TotalNotActiveUser = model.Users.Count(x => x.Status == false);
            return View(model);
        }

        public ActionResult Activate(int id)
        {
            string Status = "";

            User Data = _context.Users.Where(x => x.ID == id).FirstOrDefault();
            Data.Status = true;
            _context.SaveChanges();
            return RedirectToAction("UserIndex");
        }

        public ActionResult Deactivate(int id)
        {
            string Status = "";
            User Data = _context.Users.Where(x => x.ID == id).FirstOrDefault();
            Data.Status = false;
            _context.SaveChanges();
            return RedirectToAction("UserIndex");
        }
        public ActionResult GetDetailsUser(int? id)
        {

            User obj = new User();

            obj = (from y in _context.Users
                   where y.ID == id
                   select y).FirstOrDefault();

            return View("GetDetailsUser", obj);

        }
        [HttpGet]
        public ActionResult EditUser(int id)
        {
            return View(_context.Users.Find(id));
        }

        public ActionResult EditUser(User user)
        {
            if (user.ID == 0)
            {
                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                _context.SaveChanges();
            }
            else
            {
                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        
            return PartialView("UserItem", user);
        }

        [HttpPost]
        public IActionResult DeleteUser(int teacherID)
        {
            try
            {
                 User com = _context.Users.Where(x => x.ID == teacherID).FirstOrDefault();
                 _context.Users.Remove(com);
                _context.SaveChanges();

                
                return Ok(new { success = true });
            }

            catch (Exception)
            {
                return Json(new { success = false });
            }
        }
        private  string DecryptPasswordBase64(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private string Encrypt_Password(string password)
        {
            string pswstr = string.Empty;
            byte[] psw_encode = new byte[password.Length];
            psw_encode = System.Text.Encoding.UTF8.GetBytes(password);
            pswstr = Convert.ToBase64String(psw_encode);
            return pswstr;
        }
        public IActionResult Profile()
        {
            var userId = int.Parse(HttpContext.Session.GetString("ID"));
            var user = _context.UserLogins.Where(x => x.ID == userId).FirstOrDefault();
            return View(new Userviewmodel
            {
                Id = user.ID,
                LastName = user.LastName,
                Email = user.Email,
                UserName = user.Username,
                Phone = user.PhoneNo,
                password = DecryptPasswordBase64(user.Password),
                FirstName = user.FirstName,
            });
        }
        [HttpPost]
        public IActionResult Profile(Userviewmodel model)
        {
            var userlogin = new UserLogin();
            if (_context.UserLogins.Any(a => a.Username.Equals(model.UserName) && a.ID.Equals(model.Id)))
            {
                ViewData["messagess"] = "Please, the UserName is in the database, choose another UserName";
            }
            else
            {
                userlogin.ID = model.Id;
                userlogin.Username = model.UserName;
                userlogin.FirstName = model.FirstName;
                userlogin.LastName = model.LastName;
                userlogin.Email = model.Email;
                userlogin.PhoneNo = model.Phone;
                userlogin.Password = Encrypt_Password(model.password);
                userlogin.ResetPasswordCode = "";
                userlogin.Body = "";
                userlogin.Code = "";
                userlogin.MobileNumber = "";
                userlogin.IsValid = true;
                var result = _context.Update(userlogin);
                _context.SaveChanges();
                if (result != null)
                {
                    ViewData["message"] = "Succefully Update Profile";
                }
                else
                {
                    ViewData["message"] = "Profile Update Error";
                }
            }
            return View();
        }

 
    }
}
