﻿using Clinic_Managment.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NuGet.Protocol.Plugins;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using static Clinic_Managment.Areas.Managment.Query.StudentQuery;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
  
    public class StudentController : Controller
    {
        private readonly MediatR.ISender _mediator;
        private readonly Userdata _context;
        public StudentController(MediatR.ISender mediator, Userdata userdata )
        {
            _mediator = mediator;
            _context = userdata;
        }
        public IActionResult StudentIndex()
        {
            List<StudentModel> Studentlist = new List<StudentModel>();
            var student = _context.studentModels;
            Studentlist = (from StudentModel in student
                          select StudentModel).ToList();
            return View(Studentlist);
        }
        [HttpGet]
        public async Task<IActionResult> EditStudent(int studentid)
        {
            var student = await _mediator.Send(new EditStudentModel(studentid));
            return View(student);
        }
        [HttpPost]
        public async Task<IActionResult> EditStudent(StudentModel studentModel)
        {
            var studentmodel = await _mediator.Send(new SaveStudentModel(studentModel));
            return View("StudentItem", studentmodel);
        }
        public async Task<IActionResult> GetDetailsStudent(int studentid)
        {
            var detailstudent = await _mediator.Send(new EditStudentModel(studentid));
             return View("GetDetailsStudent", detailstudent);
        }
    }
}
