﻿using Clinic_Managment.DAL;
using Clinic_Managment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using System.Data;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    [Authorize]
    public class ProductController : Controller
    {
        Product_DAL Product_DAL = new Product_DAL();
        public IActionResult ProductIndex()
        {
            var productlist = Product_DAL.GetAllProduct();
            return View(productlist);
        }
        [HttpGet]
        public IActionResult AddOREdit(int productid)
        {
            var GetProduct = Product_DAL.GetAllProduct().Where(x=>x.ProductID == productid).FirstOrDefault();
            return View(GetProduct);
        }
        [HttpPost]
        public IActionResult AddOREdit(Product product)
        {
            if (product.ProductID > 0)
            {
                var Updated = Product_DAL.UpdateProduct(product);
                if (Updated != null)
                {
                    return PartialView("ProductItem", product);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                int Inserted = Product_DAL.AddProduct(product);
                product.ProductID = Inserted;
                if (Inserted != null)
                {
                    return PartialView("ProductItem", product);
                }
                else
                {
                    return null;
                }
            }
        }

        [HttpPost]
        public IActionResult DeleteProduct(int IDproduct)
        {
            var removeproduct = Product_DAL.DeleteProduct(IDproduct);
            if (removeproduct.Contains("deleted"))
            {
                return Ok(new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }
        public IActionResult DetailsProduct(int detailsIDproduct)
        {
            var GetProduct = Product_DAL.GetAllProduct().Where(x => x.ProductID == detailsIDproduct).FirstOrDefault();
            return View(GetProduct);
        }
        public JsonResult GetProduct(string SearchProduct)
        {
           var GetProducts = Product_DAL.getProduct(SearchProduct);
            var modifieData = GetProducts.Select(x => new
            {
                id = x.ProductID,
                text = x.ProductName
            });
            return Json(modifieData);
        }
        public IActionResult SearchProduct(string idsStr)
        {
            var searchproduct = Product_DAL.SearchProduct(idsStr);
            return PartialView("SearchProduct", searchproduct);
        }

    }
}
    