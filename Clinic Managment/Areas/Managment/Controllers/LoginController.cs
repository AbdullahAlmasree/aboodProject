﻿


using Clinic_Managment.Models;
using Grpc.Core;
using Humanizer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using NuGet.Common;
using NuGet.Protocol;
using NuGet.Protocol.Plugins;
using System.Drawing.Imaging;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Web.Helpers;
using System.Web.WebPages;
using Twilio.Clients;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio.TwiML.Voice;
using System.Security.Policy;
using System.Net;
using System.Collections.Specialized;
using System.Web;
using Clinic_Managment.Dtos;
using Clinic_Managment.Services;
using Microsoft.AspNetCore.Http.HttpResults;
using Clinic_Managment.Helpers;
using Microsoft.Extensions.Options;
using Xamarin.Essentials;
using System.Reflection;
using Twilio.TwiML.Messaging;
using static System.Net.WebRequestMethods;

namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    public class LoginController : Controller
    {

        private readonly Userdata _context;
        private readonly ISMSServices _smsServices;
        private readonly Microsoft.AspNetCore.Hosting.IHostingEnvironment _hostingEnvironment;
        private readonly TwilioSetting _twilio;
        public LoginController(Userdata context , IOptions<TwilioSetting> twilio, ISMSServices smsServices)
        {
            _context = context;
            _twilio = twilio.Value;
            _smsServices = smsServices;
        }

        [HttpPost]
        public IActionResult Send(UserLogin sendSmsDto)
        {
            if (_context.UserLogins.Any(a => a.Username.Equals(sendSmsDto.Username)))
            {
                return Ok("validio455ns");
            }
            if (_context.UserLogins.Any(a => a.MobileNumber.Equals(sendSmsDto.MobileNumber)))
            {
                return Ok("validdttdate");
            }
            Random random = new Random();
            int value = random.Next(1001, 9999);
            var Code = sendSmsDto.Code = value.ToString();
            string bodyss = "YOur Code Is" + Code;
            var body = sendSmsDto.Body = HttpUtility.UrlEncode(bodyss);
            TwilioClient.Init(_twilio.AccountSID, _twilio.AuthToken);
            var result = MessageResource.Create(
                body: body,
                from: new Twilio.Types.PhoneNumber(_twilio.TwilioPhoneNumber),
                to: sendSmsDto.MobileNumber
                );
            HttpContext.Session.SetString("ID", sendSmsDto.ID.ToString());
            HttpContext.Session.SetString("Mobile", sendSmsDto.MobileNumber);
            sendSmsDto.IsValid = false;
            sendSmsDto.ResetPasswordCode = "";
            sendSmsDto.Password = Encrypt_Password(sendSmsDto.Password);
            sendSmsDto.Email = "";
            _context.Add(sendSmsDto);
            _context.SaveChanges();
            return Ok(new { propertyName1 = "Success", result });
        }

        public IActionResult OTPVerfiction()
        {
            return View();
        }
        [HttpPost]
        public IActionResult OTPVerfiction(string dto)
        {
            var otp = _context.UserLogins.Where(x => x.Code == dto).FirstOrDefault();
            
            if (otp != null)
            {
                confirmf(dto);
            }
            else
            {
                return Ok("Falsotp");
            }
            return View();
        }
        public IActionResult confirmf(string id)
        {
            ViewBag.dto = id;
            return View();
        }
        [HttpPost]
          public IActionResult RegisterConfirms(string ID)
          {
            string id = ViewBag.dto = ID;
            UserLogin Datass = _context.UserLogins.Where(x => x.Code == id).FirstOrDefault();
            Datass.IsValid = true;
            Datass.Code = "";
            _context.SaveChanges();
            return Ok("Done");
          }

        public IActionResult SaveData(UserLogin model)
        {
            if (_context.UserLogins.Any(a => a.Username.Equals(model.Username)))
            {
                return Ok("validions");
            }
            if (_context.UserLogins.Any(a => a.Email.Equals(model.Email)))
            {
                return Ok("validate");
            }
            var models = new UserLogin();
            model.IsValid = false;
            model.ResetPasswordCode = "";
            model.MobileNumber = "";
            model.Body = "";
            model.Code = "";
            model.Password = Encrypt_Password(model.Password);
            _context.UserLogins.Add(model);
            _context.SaveChanges();
            BuildEmailTemplate(model.ID);
            return Ok("True");
        }

        public IActionResult Confirm(int regId)
        {
            ViewBag.regID = regId;
            return View();
        }
        public IActionResult RegisterConfirm(int regId)
        {
            UserLogin Data = _context.UserLogins.Where(x => x.ID == regId).FirstOrDefault();
            Data.IsValid = true;
            _context.SaveChanges();
            return Ok("Done");
        }
        public void BuildEmailTemplate(int regID)
        {
            string inputString = "EmailTemplate/Text.cshtml";
            string body = System.IO.File.ReadAllText(inputString);
            var regInfo = _context.UserLogins.Where(x => x.ID == regID).FirstOrDefault();
            var url = "https://localhost:44301" + "/Managment/Login/Confirm?regId=" + regID;
            body = body.Replace("@ViewBag.ConfirmationLink", url);
            body = body.ToString();
            BuildEmailTemplate("Your Account Is Successfully Created", body, regInfo.Email);
        }
        public static void BuildEmailTemplate(string subjectText, string bodyText, string sendTo)
        {

            string from, to, bcc, cc, subject, body;
            from = "almasreeabdullah1@outlook.com";
            to = sendTo.Trim();
            bcc = "";
            cc = "";
            subject = subjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(bodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from);
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }
       
     


        public static void SendEmail(MailMessage mail)
        {

            SmtpClient client = new SmtpClient();
            client.Host = "smtp.outlook.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential(mail.From.ToString(), "microCare#2023");

            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private string Encrypt_Password(string password)
        {
            string pswstr = string.Empty;
            byte[] psw_encode = new byte[password.Length];
            psw_encode = System.Text.Encoding.UTF8.GetBytes(password);
            pswstr = Convert.ToBase64String(psw_encode);
            return pswstr;
        }

        public IActionResult Login()
        {
            ClaimsPrincipal claimUser = HttpContext.User;
            if (claimUser.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLogin model)
        {
            var data = new Visit();
            var modelss = model.Password = Encrypt_Password(model.Password);
            var Userd = _context.UserLogins.Where(x => x.Username == model.Username && x.Password == model.Password).FirstOrDefault();
            var User = _context.UserLogins.Where(x => x.Username == model.Username).FirstOrDefault();
            if (User== null)
            {
                return Ok("msg1");
            }
            else
            {
                var Users = _context.UserLogins.Where(x => x.Password == model.Password).FirstOrDefault();

                if (Users == null)
                {
                    return Ok("msg2");
                }
                if(Userd == null)
                {
                    return Ok("msg9");
                }

            }
           
            if (Userd.IsValid == true)
            {

                var datedata = _context.UserLogins.Where(x=>x.ID == Userd.ID).FirstOrDefault();
                HttpContext.Session.SetString("LastLogin", datedata.LastLoginDate.ToString());
                var datas = Userd.LastLoginDate = DateTime.Now;
                _context.SaveChanges();
                HttpContext.Session.SetString("ID", Userd.ID.ToString());
                HttpContext.Session.SetString("username", Userd.Username);
                List<Claim> claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.NameIdentifier, model.Username),
                     new Claim("OtherProperties", "Example Role")
                };
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims,
                CookieAuthenticationDefaults.AuthenticationScheme);
                AuthenticationProperties properties = new AuthenticationProperties()
                {
                    AllowRefresh = true,
                    IsPersistent = true
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
               new ClaimsPrincipal(claimsIdentity), properties);
                return Ok("Done");
            }
            else
            {
                return Ok("result");
            }

        }
       

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Login", "Managment");
        }
        [HttpGet]
        public IActionResult ForgerMobile()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ForgerMobile(string mobileID)
        {
            string message = "";
            string messages = "";
            string msget = "";
            bool status = false;
            if(mobileID == null)
            {
                msget = "The Mobile Is Not Filled";
            }
            else
            {
                mobileID = mobileID.Trim();
                var sms = _context.UserLogins.Where(a => a.MobileNumber == mobileID).FirstOrDefault();
                if (sms != null)
                {
                    Random random = new Random();
                    int value = random.Next(1001, 9999);
                    var Code = value.ToString();
                    string bodyss = "YOur Code Is" + Code;
                    var body = sms.Body = HttpUtility.UrlEncode(bodyss);
                    TwilioClient.Init(_twilio.AccountSID, _twilio.AuthToken);

                    var result = MessageResource.Create(
                        body: body,
                        from: new Twilio.Types.PhoneNumber(_twilio.TwilioPhoneNumber),
                        to: sms.MobileNumber
                        );
                    HttpContext.Session.SetString("ID", sms.ID.ToString());
                    HttpContext.Session.SetString("Mobile", sms.MobileNumber);
                    sms.Body = body;
                    sms.Code = Code;
                    _context.SaveChanges();
                    message = "Reset password link has been sent to your Mobile";
                    return RedirectToAction("OTPVerfictions", "Login", "Managment");
                }
                else
                {
                    messages = "Mobile not found";

                }
                ViewBag.Message = message;
                ViewBag.Messages = messages;
            }
            ViewBag.Messages = msget;
            return View();
        }
        public IActionResult OTPVerfictions()
        {
            return View();
        }
        [HttpPost]
        public IActionResult OTPVerfictions(string dtos)
        {
            var otp = _context.UserLogins.Where(x => x.Code == dtos).FirstOrDefault();

            if (otp != null)
            {
                ResetPasswords(dtos);
            }
            else
            {
                return Ok("Falsotp");
            }
            return View();
        }
        public IActionResult ResetPasswords(string dtoss)
        {
            ViewBag.dtos = dtoss;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetPasswords(string ID , ResetPasswordModel model)
        {
            ViewBag.dtos = ID;
            var message = "";
            var messagfdfde = "";
            string messagConfirmPasswords = "";
            var models = Encrypt_Password(model.NewPassword);
            if (_context.UserLogins.Any(a => a.Password.Equals(models)))
            {
                messagfdfde = "Please, the Password is in the database, choose another Password";
            }
            else
            {
                if (model.NewPassword != model.ConfirmPassword)
                {
                    messagConfirmPasswords = "Please, the new password and confirm They must be similar";
                    ViewBag.messagConfirmPasswords = messagConfirmPasswords;

                }
                var user = _context.UserLogins.Where(a => a.Code == ID).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Encrypt_Password(model.NewPassword);
                        user.Code = "";
                        user.ResetPasswordCode = "";
                        _context.SaveChanges();
                        message = "New Password updated successfully";
                        return RedirectToAction("Login", "Login", new { area = "Managment" });
                    }
                else
                {
                    message = "Something invalid";
                }
                ViewBag.Message = message;
            }

            ViewBag.messagfdfde = messagfdfde;
            return View(model);

        }

        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public IActionResult ForgotPassword(string EmailID)
        {
            string message = "";
            string messages = "";
            string msget = "";
            bool status = false;

           if(EmailID == null)
            {
                msget = "Please, the Email value is not filled";
            }
            else
            {
                var account = _context.UserLogins.Where(a => a.Email == EmailID).FirstOrDefault();
                if (account != null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    send(resetCode, account.Email);
                    account.ResetPasswordCode = resetCode;
                    _context.SaveChanges();
                    message = "Reset password link has been sent to your email id";
                }
                else
                {
                    messages = "Account not found";
                }
                ViewBag.Message = message;
                ViewBag.Messages = messages;
            }
            ViewBag.msget = msget;
            return View();
        }
        public void send(string resetCode, string email)
        {
            string inputStrings = "EmailTemplate/forgot.cshtml";
            string body = System.IO.File.ReadAllText(inputStrings);
            var regInfo = _context.UserLogins.Where(x => x.ResetPasswordCode == resetCode).FirstOrDefault();
            var url = "https://localhost:44301/" + "/Managment/Login/ResetPassword?id=" + resetCode;
            body = body.Replace("@ViewBag.ConfirmationLink", url);
            body = body.ToString();
            BuildEmailTemplate(resetCode, body, email);
        }

        public IActionResult ResetPassword(string id)
        {
            string massege = "";

                var user = _context.UserLogins.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if (user != null)
                {

                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return Ok("Falses");
                }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            var messagfdfde = "";
            string messagConfirmPassword = "";
            var models = Encrypt_Password(model.NewPassword);
            if (_context.UserLogins.Any(a => a.Password.Equals(models)))
            {
                messagfdfde = "Please, the Password is in the database, choose another Password";
            }
            else
            {
                if (model.NewPassword != model.ConfirmPassword)
                {
                    messagConfirmPassword = "Please, the new password and confirm They must be similar";
                    ViewBag.messagConfirmPasswords = messagConfirmPassword;

                }
                else
                {
                    var user = _context.UserLogins.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Encrypt_Password(model.NewPassword);
                        user.ResetPasswordCode = model.ResetCode;
                        _context.SaveChanges();
                        message = "New Password updated successfully";
                        return RedirectToAction("Login", "Login", new { area = "Managment" });
                    }
                    else
                    {
                        message = "Something invalid";
                    }
                    ViewBag.Message = message;
                }
            }

            ViewBag.messagfdfde = messagfdfde;
            return View(model);

        }

    }
}

