﻿using Clinic_Managment.Dtos;
using Clinic_Managment.Helpers;
using Clinic_Managment.Models;
using Clinic_Managment.Services;
using Humanizer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Web;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;
using Twilio.AspNet.Core;
namespace Clinic_Managment.Areas.Managment.Controllers
{
    [Area("Managment")]
    public class SmsSendController : Controller
    {

        private readonly ISMSServices _smsServices;
        private readonly TwilioSetting _twilio;
        private readonly Userdata _context;
        public SmsSendController(ISMSServices smsServices  ,IOptions<TwilioSetting> twilio, Userdata context)
        {
            this._smsServices = smsServices;
            _twilio = twilio.Value;
            _context = context;
        }
        public IActionResult Send()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Send(SendSmsDto sendSmsDto)
        {
            Random random = new Random();
            int value = random.Next(1001, 9999);
            var Code = sendSmsDto.Code = value.ToString();
            string bodyss = "YOur Code Is" + Code;
            var body = sendSmsDto.Body = HttpUtility.UrlEncode(bodyss);
            TwilioClient.Init(_twilio.AccountSID, _twilio.AuthToken);
          var result = CallResource.Create(
             url: new Uri("http://demo.twilio.com/docs/voice.xml"),
              to: new Twilio.Types.PhoneNumber(sendSmsDto.MobileNumber),
              from: new Twilio.Types.PhoneNumber(_twilio.TwilioPhoneNumber)
              );
            _context.Add(sendSmsDto);
            _context.SaveChanges();
            return Ok(new { propertyName1 = "Success", result });
        }
        [HttpPost]
        public IActionResult Recvercall()
        {
            var response = new VoiceResponse();
            response.Say("hello rafed welcome to twilio");
            return new TwiMLResult (response);
        }

    }
    }
