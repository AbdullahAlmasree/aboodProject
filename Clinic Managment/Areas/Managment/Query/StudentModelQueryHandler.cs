﻿using Clinic_Managment.Areas.Managment.IRepositry;
using Clinic_Managment.Models;
using MediatR;
using static Clinic_Managment.Areas.Managment.Query.StudentQuery;

namespace Clinic_Managment.Areas.Managment.Query
{
    public class StudentModelQueryHandler : IRequestHandler<EditStudentModel, StudentModel>
    {
        public IStudentRepositry _studentRepositry;
        public StudentModelQueryHandler(IStudentRepositry studentRepositry)
        {
            _studentRepositry = studentRepositry;
        }

        public async Task<StudentModel> Handle(EditStudentModel query, CancellationToken cancellationToken)
        {
            var StudentModeEditor = _studentRepositry.EditStudentModel(query.studentid);
            return StudentModeEditor;
        }
    }

}
