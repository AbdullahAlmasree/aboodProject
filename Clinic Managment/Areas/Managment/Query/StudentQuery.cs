﻿using Clinic_Managment.Models;
using MediatR;

namespace Clinic_Managment.Areas.Managment.Query
{
    public class StudentQuery
    {
        public record EditStudentModel(int studentid) : IRequest<StudentModel>;
        public record SaveStudentModel(StudentModel StudentModel) : IRequest<StudentModel>;
    }
}
