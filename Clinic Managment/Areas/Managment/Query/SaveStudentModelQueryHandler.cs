﻿using Clinic_Managment.Areas.Managment.IRepositry;
using Clinic_Managment.Models;
using MediatR;
using static Clinic_Managment.Areas.Managment.Query.StudentQuery;

namespace Clinic_Managment.Areas.Managment.Query
{
    public class SaveStudentModelQueryHandler : IRequestHandler<SaveStudentModel, StudentModel>
    {
        public IStudentRepositry _studentRepositry;
        public SaveStudentModelQueryHandler(IStudentRepositry studentRepositry)
        {
            _studentRepositry = studentRepositry;
        }

        public async Task<StudentModel> Handle(SaveStudentModel query, CancellationToken cancellationToken)
        {
            var student = new StudentModel();
            student.StudentId = query.StudentModel.StudentId;
            student.Name = query.StudentModel.Name;
            var StudentModeEditor = _studentRepositry.SaveStudentModel(student);
            return StudentModeEditor;
        }
    }

}
