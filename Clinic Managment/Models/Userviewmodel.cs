﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Clinic_Managment.Models
{
    public class Userviewmodel
    {
        public int Id { get; set; }
        [DisplayName("FirstName")]
        public string FirstName { get; set; }
        [DisplayName("LastName")]
        public string LastName { get; set; }
        [DisplayName("E-Mail")]
        [EmailAddress(ErrorMessage ="Please enter a valid email address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DisplayName("Phone No")]
        public string Phone { get; set; }
        public IList<UserLogin> Users { get; set; }
       public bool ISlookedOUt { get; set; }
        public string password { get; set; }
        [DisplayName("UserName")]
        public string UserName { get; set; }

    }
}
