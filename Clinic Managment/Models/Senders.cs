﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic_Managment.Models
{
    public class Senders
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int SenderID { get; set; }
        public int ID { get; set; }
        public string Sender { get; set; }
        [ForeignKey("ID")]
        public virtual UserLogin UserLogins { get; set; }
    }
}
