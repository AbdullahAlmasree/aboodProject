﻿namespace Clinic_Managment.Models
{
    public class UserModel
    {

        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }

        public string Status { get; set; }
    }
}
