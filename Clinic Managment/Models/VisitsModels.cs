﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ChartJSCore.Models;
using Humanizer.Localisation;
using System.Collections.Generic;
using System.ComponentModel;

namespace Clinic_Managment.Models
{
    public class VisitsModels
    {
        public int? ID { get; set; }
        [Required(ErrorMessage = "Please enter your Date")]
        [Display(Name = "VistDate")]
        [DataType(DataType.Text)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime VistDate { get; set; }
        [Required]
        public int DoctorId { get; set; }
        [Required]
        public int PatientId { get; set; }
        [Required]
        public int ClinicID { get; set; }

        [Required(ErrorMessage = "Please enter your Patient")]
        [Display(Name = "PatientName")]
        [DataType(DataType.Text)]
        public string PatientName { get; set; }

        [Required(ErrorMessage = "Please enter your Docotor")]
        [Display(Name = "DoctorName")]
        [DataType(DataType.Text)]
        public string DoctorName { get; set; }

        [Required(ErrorMessage = "Please enter your Clinic")]
        [Display(Name = "Clinic")]
        [DataType(DataType.Text)]
        public string Clinic { get; set; }

    }


}



