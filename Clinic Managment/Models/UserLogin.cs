﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Text;

namespace Clinic_Managment.Models
{
    public class UserLogin
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }

        public string FirstName { get; set; }
        public string Username { get; set; }
        public string LastName { get; set; }
        [DisplayName("E-Mail")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Password { get; set; }
        public string MobileNumber { get; set; }
        public string Body { get; set; }
        public string Code { get; set; }
        public Nullable<bool> IsValid { get; set; }
        public string ResetPasswordCode { get; set; }
        public DateTime LastLoginDate { get; set; }
        [NotMapped]
        public string DecryptedPassword
        {
            get { return Decrypt_Password(Password); }
            set { Password = Decrypt_Password(value); }
        }

        private string Decrypt_Password(string encryptpassword)
        {
            string pswstr = string.Empty;
            System.Text.UTF8Encoding encode_psw = new System.Text.UTF8Encoding();
            System.Text.Decoder Decode = encode_psw.GetDecoder();

            byte[] todecode_byte = Encoding.ASCII.GetBytes(encryptpassword);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            pswstr = new String(decoded_char);
            return pswstr;
        }

    }
}
