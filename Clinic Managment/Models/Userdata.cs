﻿using Clinic_Managment.Dtos;
using Microsoft.EntityFrameworkCore;

namespace Clinic_Managment.Models
{
    public class Userdata : DbContext
    {
    

        public Userdata(DbContextOptions<Userdata> options)
            : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Patient> Patient { get; set; }
        public DbSet<Doctor> Doctor { get; set; }
        public DbSet<Clinic> Clinic { get; set; }
        public DbSet<StudentModel> studentModels { get; set; }
        public DbSet<SendSmsDto> SmsDtos { get; set; }
        public DbSet<Product> products { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            {
                optionsBuilder.UseSqlServer("Server=.;Database=Manageamets; Integrated Security=True; TrustServerCertificate=true");
                base.OnConfiguring(optionsBuilder);
            }
        }

   
    }
}
