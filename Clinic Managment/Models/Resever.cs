﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Clinic_Managment.Models
{
    public class Resever
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ReciverID { get; set; }
        public int ID { get; set; }
        public string Resevers { get; set; }
        [ForeignKey("ID")]
        public virtual UserLogin UserLoginss { get; set; }
    }
}
