﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Clinic_Managment.Models
{
    public class Chatmessages
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ChatMessageID { get; set; }
        public int SenderID { get; set; }
        [ForeignKey("ID")]
        public int ID { get; set; }
        public int ReciverID { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public virtual UserLogin UserLogin { get; set; }

    }
}
