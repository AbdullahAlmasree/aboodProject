﻿using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Clinic_Managment.Models
{
    public class ChangPaswordModel
    {

        [Required(ErrorMessage = "Old password required", AllowEmptyStrings = true)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "New password required", AllowEmptyStrings = true)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "New password and confirm password does not match")]
        public string ConfirmPassword { get; set; }

        public string DecryptedPassword
        {
            get { return Decrypt_Password(NewPassword); }
            set { NewPassword = Decrypt_Password(value); }
        }

        private string Decrypt_Password(string encryptpassword)
        {
            string pswstr = string.Empty;
            System.Text.UTF8Encoding encode_psw = new System.Text.UTF8Encoding();
            System.Text.Decoder Decode = encode_psw.GetDecoder();

            byte[] todecode_byte = Encoding.ASCII.GetBytes(encryptpassword);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            pswstr = new String(decoded_char);
            return pswstr;
        }
    }
}
