﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Clinic_Managment.Models
{
    public class Clinic
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int clinicID { get; set; }
        public string clinic { get; set; }
    }
}
