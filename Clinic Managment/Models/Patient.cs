﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NuGet.DependencyResolver;

namespace Clinic_Managment.Models
{
    public class Patient
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int patientId { get; set; }
        public string Name { get; set; }
     
    }
}
