﻿using Microsoft.AspNetCore.Identity;

namespace Clinic_Managment.Models
{
    public class ApplicationUser : IdentityUser
    {
        public virtual DateTime? LastLoginTime { get; set; }
        // other properties  
    }
}
