﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NuGet.DependencyResolver;

namespace Clinic_Managment.Models
{
    public class Doctor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int doctorId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Visit> Visits { get; set; }

    }
}
