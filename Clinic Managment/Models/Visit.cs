﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using ChartJSCore.Models;
using Humanizer.Localisation;
using System.Collections.Generic;
using System.ComponentModel;

namespace Clinic_Managment.Models
{

    public class Visit
    {
    
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ID { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy hh:mm tt}")]
        public DateTime VistDate { get; set; }
        [Required]
        public int doctorId { get; set; }
        [Required]
        public int patientId { get; set; }
        [Required]
        public int clinicID { get; set; }
        [Display(Name = "Doctor")]
        [DataType(DataType.Text)]
        [Required (ErrorMessage = "Please enter your Doctor")]
        public virtual Doctor Doctor { get; set; }
        [Display(Name = "Patient")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please enter your Patient")]
        public virtual Patient Patient { get; set; }
        [Display(Name = "Clinic")]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Please enter your Clinic")]
        public virtual Clinic Clinic { get; set; }

    }

    }


