﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Clinic_Managment.Models
{
    public class UserDashboardModel
    {
        public IList<User> Users { get; set; }
        public IList<UserLogin> UserLogins { get; set; }
        public int TotalActiveUser { get; set; }
        public int TotalNotActiveUser { get; set; }
    }
}
